@extends('basic_template')

@section('title', 'Special Offer: '.$specialOffer->name)

@section('breadcrumbs', Breadcrumbs::render('special-offer-show', $specialOffer))

@section('content')

<row>
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item">Name: <strong>{{ $specialOffer->name }}</strong></li>
            <li class="list-group-item">Discount: <strong>{{ $specialOffer->discount }}%</strong></li>
            <li class="list-group-item">Expiration Date: <strong>{{ $specialOffer->expiration_date->format('d.m.Y') }}</strong></li>
            <li class="list-group-item">Vouchers: <strong>{{ $specialOffer->voucher_count }}</strong></li>
        </ul>

        <div class="btn-group btn-group-justified" role="group">
            <a href="{{ route('special-offer-edit', $specialOffer) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
            <a href="" class="btn btn-default" data-toggle="modal" data-title="Special Order" data-url="{{ route('special-offer-destroy', $specialOffer) }}" data-target="#modalDelete"><span class="glyphicon glyphicon-trash"></span> Remove</a>
        </div>
    </div>
    <div class="col-md-8">
        @include('vouchers.table', ['vouchers' => $specialOffer->voucherCodes()->paginate(15)])
    </div>
</row>


@endsection