@extends('basic_template')

@section('title', 'New Special Offer')

@section('breadcrumbs', Breadcrumbs::render('special-offer-create'))

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('special-offer-store') }}" method="POST">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Name">
        </div>

        <div class="form-group">
            <label for="discount">Discount</label>
            <input type="number" min="0" max="100" class="form-control" id="discount" name="discount" value="{{ old('discount') }}" placeholder="Discount">
        </div>

        <div class="form-group">
            <label for="expirationDate">Expiration Date</label>
            <input type="text" class="form-control" id="expirationDate" name="expirationDate" value="{{ old('expirationDate') }}" placeholder="Expiration Date">
        </div>

        <button class="btn btn-primary">Create</button>
    </form>
@endsection