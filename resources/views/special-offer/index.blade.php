@extends('basic_template')

@section('title', 'Special Offers')

@section('breadcrumbs', Breadcrumbs::render('special-offers'))

@section('content')


    @include('special-offer.table', compact('specialOffers'))


@endsection