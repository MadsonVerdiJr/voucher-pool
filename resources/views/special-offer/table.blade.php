<table class="table table-striped table-hover text">
    <thead>
        <tr>
            <th colspan="3">
                <a href="{{ route('special-offer-create') }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-plus"></span> New Special Offer</a>
            </th>
            <th colspan="2">
                <form action="{{ route('special-offer') }}" method="GET" class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" id="searchParam" name="searchParam" value="{{ Request::get('searchParam') }}" placeholder="Search">
                    </div>

                    <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></button>
                </form>
            </th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Disccount</th>
            <th>Expiration Date</th>
            <th>Vouchers</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($specialOffers as $specialOffer)
        <tr>
            <td><a href="{{ route('special-offer-show', $specialOffer) }}">{{ $specialOffer->name }}</a></td>
            <td>{{ $specialOffer->discount }}</td>
            <td>{{ $specialOffer->expiration_date }}</td>
            <td>{{ $specialOffer->voucher_count }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-center">
                {{ $specialOffers->links() }}
            </td>
        </tr>
    </tfoot>
</table>
