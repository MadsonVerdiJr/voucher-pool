@extends('basic_template')

@section('title', 'Home')

@section('breadcrumbs', Breadcrumbs::render('home'))

@section('content')
    <div>
        <div class="col-md-12">
            <a href="{{ route('special-offer-create') }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-plus"></span> New Special Offer</a>
        </div>
    </div>

    @include('vouchers.table', ['vouchers' => $vouchers])

@endsection
