<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>
                <a href="{{ route('recipients-create') }}" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-plus"></span> New Recipient</a>
            </th>
            <th>
                <form action="{{ Request::fullUrl() }}" method="GET" class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" id="searchParam" name="searchParam" value="{{ Request::get('searchParam') }}" placeholder="Search recipients">
                    </div>

                    <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></button>
                </form>
            </th>
        </tr>
        <tr>
            <th>E-mail</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($recipients as $recipient)
            <tr>
                <td><a href="{{ route('recipients-show', $recipient) }}">{{ $recipient->email }}</a></td>
                <td>{{ $recipient->name }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">
                {{ $recipients->links() }}
            </td>
        </tr>
    </tfoot>
</table>
