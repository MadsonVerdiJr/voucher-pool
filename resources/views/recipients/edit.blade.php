@extends('basic_template')

@section('title', 'Edit Recipient: '.$recipient->name)

@section('breadcrumbs', Breadcrumbs::render('recipients-edit', $recipient))

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('recipients-update', $recipient) }}" method="POST">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $recipient->name) }}" placeholder="Name">
        </div>

        <div class="form-group">
            <label for="email">Discount</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $recipient->email) }}" placeholder="E-mail">
        </div>

        <button class="btn btn-primary">Update</button>
    </form>
@endsection