@extends('basic_template')

@section('title', 'Recipient: '.$recipient->name)

@section('breadcrumbs', Breadcrumbs::render('recipients-show', $recipient))

@section('content')

<row>
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item">Name: <strong>{{ $recipient->name }}</strong></li>
            <li class="list-group-item">E-mail: <strong>{{ $recipient->email }}</strong></li>
        </ul>

        <div class="btn-group btn-group-justified" role="group">
            <a href="{{ route('recipients-edit', $recipient) }}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span> Edit</a>
            <a href="" class="btn btn-default" data-toggle="modal" data-title="Recipient" data-url="{{ route('recipients-destroy', $recipient) }}" data-target="#modalDelete"><span class="glyphicon glyphicon-trash"></span> Remove</a>
        </div>
    </div>
    <div class="col-md-8">
        @include('vouchers.table_offer', compact('vouchers'))
    </div>
</row>


@endsection
