@extends('basic_template')

@section('title', 'Recipients')

@section('breadcrumbs', Breadcrumbs::render('recipients'))

@section('content')


    @include('recipients.table', compact('recipients'))


@endsection