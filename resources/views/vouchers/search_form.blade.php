<form action="{{ Request::fullUrl() }}" method="GET" class="form-inline">
    <div class="input-group">
        <input type="text" class="form-control" id="searchParam" name="searchParam" value="{{ Request::get('searchParam') }}" placeholder="Search vouchers">
    </div>

    <button class="btn btn-primary"><span class="glyphicon glyphicon-search"></button>
</form>
