@extends('basic_template')

@section('title', 'Voucher: '.$voucherCode->uuid)

@section('breadcrumbs', Breadcrumbs::render('vouchers-show', $voucherCode))

@section('content')

<row>
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item">UUID: <strong>{{ $voucherCode->uuid }}</strong></li>
            <li class="list-group-item">Expiration Date: <strong>{{ $voucherCode->specialOffer->expiration_date->format('d.m.Y') }}</strong></li>
            <li class="list-group-item">Used: <strong>{{ $voucherCode->is_used ? 'Yes' : 'No' }}</strong></li>
            <li class="list-group-item">Recipient: <strong><a href="{{ route('recipients-show', $voucherCode->recipient) }}">{{ $voucherCode->recipient->email }}</a></strong></li>
            <li class="list-group-item">Special Offer: <strong><a href="{{ route('special-offer-show', $voucherCode->specialOffer) }}">{{ $voucherCode->specialOffer->name }}</a></strong></li>
        </ul>

        <a href="" class="btn btn-default btn-block" data-toggle="modal" data-title="Recipient" data-url="{{ route('voucher-codes-destroy', $voucherCode) }}" data-target="#modalDelete"><span class="glyphicon glyphicon-trash"></span> Remove</a>
    </div>
</row>


@endsection