<table class="table table-striped table-hover text">
    <thead>
        <tr>
            <th colspan="2">
                @include('vouchers.search_form')
            </th>
        </tr>
        <tr>
            <th>
                Voucher CODE
            </th>
            <th>
                Used?
            </th>
            <th>
                Date
            </th>
            <th>
                Special Offer
            </th>
        </tr>
    </thead>
    <tbody>
    @foreach ($vouchers as $voucher)
        <tr class="@if ($voucher->is_used) success @endif">
            <td>
                <a href="{{ route('voucher-codes-show', $voucher) }}">{{ $voucher->uuid }}</a>
            </td>
            <td>
                <span class="glyphicon glyphicon-{{ $voucher->is_used ? 'ok' : 'minus' }}"></span>

            </td>
            <td>
                @if ($voucher->is_used)
                    {{ $voucher->used_date->format('d.m.Y') }}
                @endif
            </td>
            <td>
                <a href="{{ route('special-offer-show', $voucher->specialOffer) }}">{{ $voucher->specialOffer->name }}</a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-center">
                {{ $vouchers->links() }}
            </td>
        </tr>
    </tfoot>
</table>
