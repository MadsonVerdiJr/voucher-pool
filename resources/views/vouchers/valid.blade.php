@extends('basic_template')

@section('title', 'Voucher Code Verify')

@section('breadcrumbs', Breadcrumbs::render('vouchers-verify'))

@section('content')

    <h2 class="page-header">
        Your Voucher Code is valid
    </h2>

    <div class="col-md-4 col-md-offset-4">
        <ul class="list-group">
            <li class="list-group-item">UUID: <strong>{{ $voucherCode->uuid }}</strong></li>
            <li class="list-group-item">Expiration Date: <strong>{{ $voucherCode->specialOffer->expiration_date->format('d.m.Y') }}</strong></li>
            <li class="list-group-item">Used: <strong>{{ $voucherCode->is_used ? 'Yes ('.$voucherCode->used_date->format('d.m.Y H:i').')' : 'No' }}</strong></li>
            <li class="list-group-item">Special Offer: <strong>{{ $voucherCode->specialOffer->name }}</strong></li>
            <li class="list-group-item">Discount: <strong>{{ $voucherCode->specialOffer->discount }}</strong></li>
        </ul>
    </div>
@endsection
