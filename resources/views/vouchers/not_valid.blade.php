@extends('basic_template')

@section('title', 'Voucher Code Verify')

@section('breadcrumbs', Breadcrumbs::render('vouchers-verify'))

@section('content')

    <h2 class="page-header text-danger">
        Your Voucher Code is NOT valid
    </h2>

@endsection
