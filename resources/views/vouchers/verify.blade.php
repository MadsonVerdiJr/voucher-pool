@extends('basic_template')

@section('title', 'Voucher Code Verify')

@section('breadcrumbs', Breadcrumbs::render('vouchers-verify'))

@section('content')

    <form action="{{ route('voucher-codes-verify-process') }}" method="POST">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
        </div>

        <div class="form-group">
            <label for="voucherCode">Voucher Code</label>
            <input type="text" class="form-control" id="voucherCode" name="voucherCode" placeholder="Voucher Code">
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">

                <button class="btn btn-primary btn-block">Verify</button>
            </div>
        </div>
    </form>
@endsection
