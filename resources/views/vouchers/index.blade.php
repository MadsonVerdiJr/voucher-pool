@extends('basic_template')

@section('title', 'Voucher Codes')

@section('breadcrumbs', Breadcrumbs::render('vouchers'))

@section('content')


    @include('vouchers.table', compact('vouchers'))


@endsection