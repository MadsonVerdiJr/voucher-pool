<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SpecialOffer extends Model
{
    protected $fillable = [
        'name',
        'discount',
        'expiration_date',
    ];

    protected $dates = [
        'expiration_date',
    ];

    // protected $dateFormat = 'd.m.Y';

    public $timestamps = false;

    /**
     * get 'voucher_count' attribute
     * All the voucher codes from this Special Offer
     * @return integer
     */
    public function getVoucherCountAttribute()
    {
        return $this->voucherCodes()->count();
    }

    /**
     * Convert from pattern 'd.m.Y' to carbon
     * @param string $value
     */
    public function setExpirationDateAttribute($value)
    {
        $this->attributes['expiration_date'] = Carbon::createFromFormat('d.m.Y', $value)->endOfDay();
    }

    /**
     * Voucher Codes from this Special Offer
     * @return App\VoucherCode
     */
    public function voucherCodes()
    {
        return $this->hasMany('App\VoucherCode', 'special_offers_id', 'id');
    }

    /**
     * Scope a query to filter by a given param.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $param)
    {
        if (is_numeric($param)) {
            $query = $query->where('discount', $param);
        } elseif (preg_match("/^[0-9]{2}\.(0[1-9]|1[0-2])\.([1-9]{4})$/", $param)) {
            $date = Carbon::createFromFormat('d.m.Y', $param)->endOfDay();
            $query = $query->whereDate('expiration_date', '=' , $date->format('Y-m-d'));
        } else {
            $query = $query->where('name', 'like', $param);
        }

        return $query;
    }
}
