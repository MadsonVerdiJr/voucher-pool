<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    protected $keyType = 'string';

    protected $primaryKey = 'email';

    protected $fillable = ['email', 'name'];

    public $incrementing = false;

    public $timestamps = false;

    /**
     * Retrieve Voucher Codes from this recipient
     * @return App\VoucherCode
     */
    public function voucherCodes()
    {
        return $this->hasMany('App\VoucherCode', 'recipients_email', 'email');
    }

    /**
     * Scope a query to filter by a given param.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $param)
    {
        if (preg_match("/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $param)) {
            $query = $query->where('email', 'like', '%'.$param.'%');
        } else {
            $query = $query->where('name', 'like', '%'.$param.'%');
        }
        // ^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$
        return $query;
    }
}
