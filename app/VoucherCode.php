<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VoucherCode extends Model
{
    protected $keyType = 'string';

    protected $primaryKey = 'uuid';

    protected $fillable = [
        'uuid',
        'used_date',
        'recipients_email'
    ];

    protected $dates = [
        'used_date'
    ];

    public $timestamps = false;

    /**
     * A shortcut for discount attribute
     * @return integer
     */
    public function getDiscountAttribute()
    {
        return $this->specialOffer->discount;
    }

    /**
     * Get is_used attributes
     * if this voucher code was used
     *
     * @return boolean
     */
    public function getIsUsedAttribute()
    {
        return $this->used_date ? true : false;
    }

    /**
     * Special Offer from this voucher code
     *
     * @return App\SpecialOffer
     */
    public function specialOffer()
    {
        return $this->belongsTo('App\SpecialOffer', 'special_offers_id', 'id');
    }

    /**
     * Recipient from this voucher code
     *
     * @return App\Recipient
     */
    public function recipient()
    {
        return $this->belongsTo('App\Recipient', 'recipients_email', 'email');
    }

    /**
     * Scope a query to filter by a given param.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $param)
    {
        if (is_numeric($param)) {
            $query = $query->whereHas('specialOffer', function ($specialOffer) use ($param)  {
                return $specialOffer->whereDiscount($param);
            });
        } elseif (preg_match("/^[0-9]{2}\.(0[1-9]|1[0-2])\.([0-9]{4})$/", $param)) {
            $date = Carbon::createFromFormat('d.m.Y', $param)->endOfDay();
            $query = $query->whereHas('specialOffer', function ($specialOffer) use ($date) {
                return $specialOffer->whereDate('expiration_date', '=' , $date->format('Y-m-d'));
            });
        } elseif (preg_match("/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/", $param)) {
            $query = $query->where('recipients_email', 'like', '%'.$param.'%');
        } else {
            $query = $query->where('uuid', 'like', '%'.$param.'%');
        }
        // ^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$
        return $query;
    }
}
