<?php

namespace App\Observers;

use Faker\Generator as Faker;
use App\VoucherCode;

class VoucherCodeObserver
{

    public function saving (VoucherCode $voucher_code) {

        if (!$voucher_code->uuid) {
            $uuid = Faker()->uuid;
            Log::debug('uuid', [$uuid]);

            $voucher_code->uuid = $uuid;
        }
    }
}