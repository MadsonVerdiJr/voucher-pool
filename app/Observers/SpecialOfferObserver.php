<?php

namespace App\Observers;

use App\SpecialOffer;
use App\Recipient;
use Faker\Generator as Faker;

class SpecialOfferObserver
{
    protected $faker;

    /**
     * Create a new factory instance.
     *
     * @param  \Faker\Generator  $faker
     * @return void
     */
    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Action after creating an Special Offer
     * @param  SpecialOffer $specialOffer
     * @return void
     */
    public function created (SpecialOffer $specialOffer)
    {
        foreach (Recipient::all() as $recipient) {
            $specialOffer->voucherCodes()->create([
                'uuid' => $this->faker->uuid,
                'recipients_email' => $recipient->email
            ]);
        }
    }

    /**
     * Action before deleting an Special Offer
     * @param  SpecialOffer $specialOffer
     * @return void
     */
    public function deleting (SpecialOffer $specialOffer)
    {
        $specialOffer->voucherCodes()->delete();
    }
}