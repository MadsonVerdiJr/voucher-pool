<?php

namespace App\Observers;


use App\Recipient;

class RecipientObserver
{
    /**
     * Action before deleting an Recipient
     * @param  SpecialOffer $specialOffer
     * @return void
     */
    public function deleting (Recipient $recipient)
    {
        $recipient->voucherCodes()->delete();
    }
}
