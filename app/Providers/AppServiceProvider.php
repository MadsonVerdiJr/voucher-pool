<?php

namespace App\Providers;

use App\Recipient;
use App\SpecialOffer;
use App\VoucherCode;
use App\Observers\SpecialOfferObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        SpecialOffer::observe(SpecialOfferObserver::class);
        VoucherCode::observe(VoucherCodeObserver::class);
        Recipient::observe(RecipientObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
