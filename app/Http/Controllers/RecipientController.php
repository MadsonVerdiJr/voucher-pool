<?php

namespace App\Http\Controllers;

use App\Recipient;
use Illuminate\Http\Request;

class RecipientController extends Controller
{
    protected $validationRules = [
        'name' => 'required|max:120',
        'email' => 'required|email|max:120',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recipients = Recipient::orderBy('email');

        if ($request->has('searchParam') and !empty($request->searchParam)) {
            $recipients->search($request->searchParam);
        }

        $recipients = $recipients->paginate(15);

        return $request->ajax() ? response($recipients, 200) : view('recipients.index', compact('recipients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->validationRules);

        $recipient = Recipient::create([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return $request->ajax() ? response($recipient, 201) : redirect()->route('recipients-show', compact('recipient'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Recipient $recipient)
    {
        $vouchers = $recipient->voucherCodes();

        if ($request->has('searchParam') and !empty($request->searchParam)) {
            $vouchers->search($request->searchParam);
        }

        $vouchers = $vouchers->paginate(15);

        return view('recipients.show', compact('recipient', 'vouchers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipient $recipient)
    {
        return view('recipients.edit', compact('recipient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipient $recipient)
    {
        $request->validate($this->validationRules);

        $recipient->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route('recipients-show', compact('recipient'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipient $recipient)
    {
        $recipient->delete();

        return redirect()->route('recipients');
    }
}
