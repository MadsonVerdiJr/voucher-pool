<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Recipient;
use App\VoucherCode;
use Illuminate\Http\Request;

class VoucherCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vouchers = VoucherCode::orderBy('used_date', 'DESC');

        if ($request->has('searchParam') and !empty($request->searchParam)) {
            $vouchers->search($request->searchParam);
        }

        $vouchers = $vouchers->paginate(15);

        return $request->ajax() ? response($vouchers, 200) : view('vouchers.index', compact('vouchers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VoucherCode  $voucherCode
     * @return \Illuminate\Http\Response
     */
    public function show(VoucherCode $voucherCode)
    {
        return view('vouchers.show', compact('voucherCode'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VoucherCode  $voucherCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(VoucherCode $voucherCode)
    {
        if (!$voucherCode->is_used) {
            $voucherCode->delete();
        }

        return redirect()->route('voucher-codes');
    }

    /**
     * Page for voucher verification
     *
     * @return Illuminate\Http\Response
     */
    public function verifyPage()
    {
        return view('vouchers.verify');
    }

    /**
     * Process the verify request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verifyProcess(Request $request)
    {
        // return $request;
        return redirect()->route('voucher-codes-verify', [
            'voucherCodes' => $request->voucherCode,
            'recipients' => $request->email,
        ]);
    }

    /**
     * Verify if voucher code is valid and return the discount
     *
     * @param  VoucherCode $voucherCode
     * @param  Recipient   $recipient
     * @return Illuminate\Http\Response
     */
    public function verify(Request $request, VoucherCode $voucherCode, Recipient $recipient)
    {
        // return $voucherCode;
        if ($recipient->voucherCodes->contains($voucherCode->uuid)) {
            if (!$voucherCode->is_used) {
                $voucherCode->update(['used_date' => Carbon::now()]);
            }
            if ($request->ajax()) {
                return ['Discount' => $voucherCode->discount];
            } else {
                return view('vouchers.valid', compact('voucherCode', 'recipient'));
            }
        } else {
            if ($request->ajax()) {
                return response('Not valid', 404);
            } else {
                return view('vouchers.not_valid');
            }
        }
    }
}
