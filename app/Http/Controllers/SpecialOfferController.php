<?php

namespace App\Http\Controllers;

use App\SpecialOffer;
use Illuminate\Http\Request;

class SpecialOfferController extends Controller
{
    protected $validationRules = [
        'name' => 'required|max:45',
        'discount' => 'required|min:0|max:100',
        'expirationDate' => 'required|date_format:d.m.Y',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $specialOffer = SpecialOffer::orderBy('expiration_date', 'DESC');

        if ($request->has('searchParam') and !empty($request->searchParam)) {
            $specialOffer->search($request->searchParam);
        }

        $specialOffers = $specialOffer->paginate(15);

        return $request->ajax() ? response($specialOffers, 200) : view('special-offer.index', compact('specialOffers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('special-offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->validationRules);

        $specialOffer = SpecialOffer::create([
            'name' => $request->name,
            'discount' => $request->discount,
            'expiration_date' => $request->expirationDate,
        ]);

        return $request->ajax() ? response($specialOffer, 201) : redirect()->route('special-offer-show', ['id' => $specialOffer->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecialOffer  $specialOffer
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialOffer $specialOffer)
    {
        return view('special-offer.show', compact('specialOffer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecialOffer  $specialOffer
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialOffer $specialOffer)
    {
        return view('special-offer.edit', compact('specialOffer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpecialOffer  $specialOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecialOffer $specialOffer)
    {
        $request->validate($this->validationRules);

        $specialOffer->update([
            'name' => $request->name,
            'discount' => $request->discount,
            'expiration_date' => $request->expirationDate,
        ]);

        return redirect()->route('special-offer-show', ['id' => $specialOffer->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialOffer  $specialOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialOffer $specialOffer)
    {
        if ($specialOffer->used_vouchers == 0) {

            $specialOffer->delete();

            return redirect()->route('special-offer');
        } else {

            return redirect()->route('special-offer');
        }

    }
}
