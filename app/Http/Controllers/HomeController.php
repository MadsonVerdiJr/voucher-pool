<?php

namespace App\Http\Controllers;

use App\VoucherCode;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Home page of the system
     *
     * @return view
     */
    public function getIndex()
    {
        $vouchers = VoucherCode::paginate(15);
        return view('home', compact('vouchers'));
    }
}
