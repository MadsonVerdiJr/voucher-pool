<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

// Home > Recipients
Breadcrumbs::register('recipients', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Recipients', route('recipients'));
});

// Home > Recipients > Show
Breadcrumbs::register('recipients-show', function ($breadcrumbs, $recipient) {
    $breadcrumbs->parent('recipients');
    $breadcrumbs->push($recipient->name, route('recipients-show', $recipient));
});

// Home > Recipients > Show
Breadcrumbs::register('recipients-edit', function ($breadcrumbs, $recipient) {
    $breadcrumbs->parent('recipients-show', $recipient);
    $breadcrumbs->push('Edit', route('recipients-edit', $recipient));
});

// Home > Recipients > Create
Breadcrumbs::register('recipients-create', function ($breadcrumbs) {
    $breadcrumbs->parent('recipients');
    $breadcrumbs->push('New Recipient', route('recipients-create'));
});

// Home > Special Offers
Breadcrumbs::register('special-offers', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Special Offers', route('special-offer'));
});

// Home > Special Offer > Create
Breadcrumbs::register('special-offer-create', function ($breadcrumbs) {
    $breadcrumbs->parent('special-offers');
    $breadcrumbs->push('New Special Offer', route('special-offer-create'));
});

// Home > Special Offer > Show
Breadcrumbs::register('special-offer-show', function ($breadcrumbs, $specialOffer) {
    $breadcrumbs->parent('special-offers');
    $breadcrumbs->push($specialOffer->name, route('special-offer-show', $specialOffer));
});

// Home > Vouchers
Breadcrumbs::register('vouchers', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Vouchers', route('voucher-codes'));
});

// Home > Vouchers > Show
Breadcrumbs::register('vouchers-show', function ($breadcrumbs, $voucher) {
    $breadcrumbs->parent('vouchers');
    $breadcrumbs->push($voucher->uuid, route('voucher-codes-show', $voucher));
});

// Home > Vouchers > Show
Breadcrumbs::register('vouchers-verify', function ($breadcrumbs) {
    $breadcrumbs->parent('vouchers');
    $breadcrumbs->push('Verify', route('voucher-codes-verify-page'));
});

// Home > About
// Breadcrumbs::register('about', function ($breadcrumbs) {
//     $breadcrumbs->parent('home');
//     $breadcrumbs->push('About', route('about'));
// });

// // Home > Blog
// Breadcrumbs::register('blog', function ($breadcrumbs) {
//     $breadcrumbs->parent('home');
//     $breadcrumbs->push('Blog', route('blog'));
// });

// // Home > Blog > [Category]
// Breadcrumbs::register('category', function ($breadcrumbs, $category) {
//     $breadcrumbs->parent('blog');
//     $breadcrumbs->push($category->title, route('category', $category->id));
// });

// // Home > Blog > [Category] > [Post]
// Breadcrumbs::register('post', function ($breadcrumbs, $post) {
//     $breadcrumbs->parent('category', $post->category);
//     $breadcrumbs->push($post->title, route('post', $post));
// });
