<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getIndex')->name('home');

////////////////////
// Special Offers //
////////////////////
Route::group(['prefix' => 'special-offer'], function() {
    Route::get('/', 'SpecialOfferController@index')->name('special-offer');
    Route::get('create', 'SpecialOfferController@create')->name('special-offer-create');
    Route::post('store', 'SpecialOfferController@store')->name('special-offer-store');

    Route::group(['prefix' => '{specialOffer}'], function() {
        Route::get('show', 'SpecialOfferController@show')->name('special-offer-show');
        Route::get('destroy', 'SpecialOfferController@destroy')->name('special-offer-destroy');
        Route::get('edit', 'SpecialOfferController@edit')->name('special-offer-edit');
        Route::post('update', 'SpecialOfferController@update')->name('special-offer-update');
    });
});

////////////////
// Recipients //
////////////////
Route::group(['prefix' => 'recipients'], function() {
    Route::get('/', 'RecipientController@index')->name('recipients');
    Route::get('create', 'RecipientController@create')->name('recipients-create');
    Route::post('store', 'RecipientController@store')->name('recipients-store');

    Route::group(['prefix' => '{recipients}'], function() {
        Route::get('show', 'RecipientController@show')->name('recipients-show');
        Route::get('destroy', 'RecipientController@destroy')->name('recipients-destroy');
        Route::get('edit', 'RecipientController@edit')->name('recipients-edit');
        Route::post('update', 'RecipientController@update')->name('recipients-update');
    });
});

///////////////////
// Voucher Codes //
///////////////////
Route::group(['prefix' => 'voucher-codes'], function() {
    Route::get('/', 'VoucherCodeController@index')->name('voucher-codes');

    Route::group(['prefix' => '{voucherCodes}'], function() {
        Route::get('show', 'VoucherCodeController@show')->name('voucher-codes-show');
        Route::get('destroy', 'VoucherCodeController@destroy')->name('voucher-codes-destroy');
    });
});


////////////
// Verify //
////////////
Route::group(['prefix' => 'verify'], function() {
    Route::get('/', 'VoucherCodeController@verifyPage')->name('voucher-codes-verify-page');
    Route::post('/', 'VoucherCodeController@verifyProcess')->name('voucher-codes-verify-process');
    Route::get('{voucherCodes}/{recipients}', 'VoucherCodeController@verify')->name('voucher-codes-verify');
});
