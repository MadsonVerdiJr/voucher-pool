<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_codes', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->dateTime('used_date')->nullable();
            $table->integer('special_offers_id')->unsigned();
            $table->string('recipients_email', 120);

            $table->primary('uuid');
            $table->foreign('special_offers_id')->references('id')->on('special_offers')->onDelete('cascade');
            $table->foreign('recipients_email')->references('email')->on('recipients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_codes');
    }
}
