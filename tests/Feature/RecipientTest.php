<?php

namespace Tests\Feature;

use App\Recipient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecipientTest extends TestCase
{
    /**
     * Register test
     * @return void
     */
    public function testRegister()
    {
        $recipient = factory(Recipient::class)->make();

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ])->json('POST', '/recipients/store', [
            'name' => $recipient->name,
            'email' => $recipient->email
        ]);

        $response->assertStatus(201);
    }

    /**
     * Retrieve one user
     * @return void
     */
    public function testRetriveUnique()
    {
        $recipient = factory(Recipient::class)->create();

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ])->json('GET', '/recipients/'.$recipient->email.'/show');

        $response->assertStatus(200);
    }
}
