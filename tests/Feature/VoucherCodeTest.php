<?php

namespace Tests\Feature;

use App\Recipient;
use App\SpecialOffer;
use App\VoucherCode;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VoucherCodeTest extends TestCase
{
    /**
     * Register test
     * @return void
     */
    public function testVerify()
    {
        $recipient = factory(Recipient::class)->create();
        $specialOffer = factory(SpecialOffer::class)->create();
        $voucherCode = $recipient->voucherCodes()->first();

        $recipient->email;
        $voucherCode->uuid;

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ])->json('GET', '/verify/'.$voucherCode->uuid.'/'.$recipient->email.'');

        $response->assertStatus(200);
    }
}
