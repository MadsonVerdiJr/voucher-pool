<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\SpecialOffer;
class SpecialOfferTest extends TestCase
{
    /**
     * Register test
     * @return void
     */
    public function testRegister()
    {
        $specialOffer = factory(SpecialOffer::class)->make();

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ])->json('POST', '/special-offer/store', [
            'name' => $specialOffer->name,
            'discount' => $specialOffer->discount,
            'expirationDate' => $specialOffer->expiration_date->format('d.m.Y'),
        ]);

        $response->assertStatus(201);
    }

    /**
     * Retrieve one user
     * @return void
     */
    public function testRetriveUnique()
    {
        $specialOffer = factory(SpecialOffer::class)->create();

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ])->json('GET', '/special-offer/'.$specialOffer->id.'/show');

        $response->assertStatus(200);
    }
}
