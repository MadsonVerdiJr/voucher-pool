# Voucher Pool

v0.14.0

## Getting Started

Using [docker](https://www.docker.com) and [docker-compose](https://docs.docker.com/compose/):

`$ cd laradock`

`$ docker-compose up -d nginx mysql`


## Postman requests

### Verify Voucher

GET request for `[HOST]/verify/[voucher-code]/[email]`

```
GET /verify/03b8ffcb-9146-3dae-ba55-c1b19e153ebd/xabbott@example.net HTTP/1.1
Host: localhost
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: 9879337f-d766-7249-1175-8f1ca23ebb60

```


### Register new Special Offer

POST request for `[HOST]/special-offer/store`

```
POST /special-offer/store HTTP/1.1
Host: localhost
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: fc108f45-52c0-675f-875b-7dbe6d42f672

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="name"

Teste
------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="discount"

5
------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="expirationDate"

10.08.2020
------WebKitFormBoundary7MA4YWxkTrZu0gW--
```


### Retrieve Special Offers


GET request for `[HOST]/special-offer?page=[page_number]`

```
GET /special-offer?page=3 HTTP/1.1
Host: localhost
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: 6e7a1070-1e43-3034-94cc-297ea9ce71a7

```

### Retrieve Vouchers


GET request for `[HOST]/voucher-codes?page=[page_number]`

```
GET /voucher-codes?page=2 HTTP/1.1
Host: localhost
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: d394baec-803a-161e-d038-f512ff3310e8

```

### Register Recipients

POST request for `[HOST]/recipients/store`

```
POST /recipients/store HTTP/1.1
Host: localhost
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: a77238f2-9245-9115-b10b-3e93a7fb41b5

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="name"

Teste
------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="email"

asd@asm.com
------WebKitFormBoundary7MA4YWxkTrZu0gW--
```

### Retrieve Recipients

GET request for `[HOST]/recipients?page=[page_number]`

```
GET /recipients?page=2 HTTP/1.1
Host: localhost
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Cache-Control: no-cache
Postman-Token: 04a5c145-7c77-18a1-30de-4173dbae386d

```
